from django.contrib import admin

# Register your models here.
#因為student定義在models.py，所以需要import來加入該套件
from cmd1.models import student,comend


 
#下面會以register的方式，將建立的資料模型向admin註冊
#第三種方式：加入ModelAdmin 類別，定義顯示欄位、欄位過濾資料、搜尋和排序
class studentAdmin(admin.ModelAdmin):
    list_display=('id','cName','cSex','cBirthday','cEmail','cPhone','cAddr')
    list_filter=('cName','cSex')
    search_fields=('cName',)
    ordering=('id',)
admin.site.register(student,studentAdmin)

class comendAdmin(admin.ModelAdmin):
    list_display=('id','cName','cmmd',)
    list_filter=('cName',)
    search_fields=('cName',)
    ordering=('id',)
admin.site.register(comend,comendAdmin)