# Generated by Django 2.1.4 on 2019-01-03 22:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmd1', '0006_auto_20190104_0536'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='cAddr',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='cEmail',
            field=models.EmailField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='cPhone',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
