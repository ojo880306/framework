from django.db import models
from collections import namedtuple
from django.db import connection



 
class student(models.Model):
    objects = models.Manager()
    cName = models.CharField(max_length=20, null=False) #建立字串型別的欄位，最大長度為20字元，欄位不可空白
    cSex = models.CharField(max_length=2, default='M', null=False) #default='M'，設定預設值為'M'
    cBirthday = models.DateField(auto_now_add=True)
    cEmail = models.EmailField(max_length=100,blank=True, default='')#blank=True 欄位可空白
    cPhone = models.CharField(max_length=50,blank=True, default='')
    cAddr = models.CharField(max_length=255,blank=True, default='')
 
    #每一筆資料在管理介面顯示的內容以下列程式定義
   # def __str__(self):
 #     return self.cName #表示顯示cName欄位
    



class comend(models.Model):
    objects = models.Manager()
    cName = models.CharField(max_length=20, null=False ,default="anc1") #建立字串型別的欄位，最大長度為20字元，欄位不可空白
    cmmd = models.CharField(max_length=255,blank=True, default='commd pls')
   
   
    def __str__(self):
         return self.cName #表示顯示cName欄位   
        